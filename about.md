---
layout: page
title: About
permalink: /about/
---

The Disabled Communities are a group of communities dedicated to helping out people who are mentally disabled by providing support groups, teaming up with other people who have the same disability, and fighting for the rights of the oppressed, as well as promoting true happiness in life.



This website was built with Jekyll.

This website was created with the [MaterializeCSS](http://materializecss.com) theme.
You can find the source code for Jekyll [here](https://github.com/jekyll/jekyll).