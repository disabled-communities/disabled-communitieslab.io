---
layout: post
title: "We Changed Our Name"
date: 2017-11-06 14:00:00 -0600
categories: name update
---

Greetings to everyone!

We decided to change our name from the UMDC (The Union of Mentally Disabled [People's] Communities) to just The Disabled Communities. The simplification of our brand allows better communiciation between our viewers, makes paperwork easier for our communities whenever we need to fill it out, lets us to go by just two initials (DC) instead of four, gets our point across in a much faster way, and unifies our brand. As our launching day (November 25th) comes closer and closer, our team has been working extra hard on making sure our communities are best for those with disabilities.
Be sure to check back on our website for more information about our upcoming services and a brand new community provided by one of our staff members. No, as always we will not be releasing any spoilers until opening day. Thanks for reading our blog posts and we'll be sure to see you next time.

Auf Wiedersehen