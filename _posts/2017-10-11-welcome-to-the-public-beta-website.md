---
layout: post
title: "Welcome To the Public Beta Website!"
date: 2017-10-11 15:00:00 -0600
categories: jekyll update
---

We are so excited to bring to you the official opening of the UMDC Public Beta 1. With our number one priority being the disabled, our union is ready to provide the best services for those in need. Comprising of The SFC Group, SFC-Aclevo, Deleno, Treehouse, and the TKPC; we have agreed to work together to make the world a better place with some suprises just waiting for the **25th of November**, our official opening day. Please be sure to come back to our blog as we talk more about updates and improvements to our services. (No, we aren't spoiling it.)

Until next time:

Auf Wiedersehen (Goodbye in German)