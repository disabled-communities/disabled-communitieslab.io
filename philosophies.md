---
layout: page
title: Philosophies
permalink: "/philosophies/"
---
Unlike some communities that help the disabled, our philosophies are based on the struggles and the suffering that Reasonably Selenium had to deal with in the real world. The stress that came from his disabilities inspired him to create his own ideologies unlike the others. They boil down to the following: Anti-Conformism, and Anti-Normieism. He always believed that dictatorship was never the answer to the problems that people face. In fact, he still does today. Below is an explaination of what these philosophies are and why they matter to the disabled.

### Anti-Conformism

Anti-Conformism is defined as being different from other people, and being yourself. In it's and the DC's definition, Anti-Conformism means not going to places where you feel unconfortable. For instance, if your friends wants to force you to go to a concert with him and you don't like loud noises and bright lights, you tell them they can go screw themselves. The truth is that this person is not your friend if their goal is to make you uncomfortable. It also means fighting against the oppressive means to drag people out of their comfort zones. For example, Reasonably Selenium took a two week "vacation" from school to show his school he wasn't going to handle dealing with huge crowds and noisy places.

### Anti-Normieism

Anti-Normieism expands the idea of being yourself to doing things other people wouldn't do. It means expressing the right to have one's own opinion regardless of what the others say. We believe that you should be able to have your own ideas without being discriminated by other communities. We embrace and fully respect the cultures of other people and honor those who resist. For instance, we should not have to like pop music because others do.

### Need Help?

The Disabled Communities are always happy to help others who feel oppressed and want to be happier about themselves. Starting November 25th, we will update our website with services and other resources to help those in need achieve better lives, one helpful hand at a time. A contact form will be opening soon for those who wish to contact us about a problem or a situation. If you are in dire need of help, please contact Reasonably Selenium on Discord. It's a free messaging app that allows people to connect with each other.

Reasonably Selenium's Discord: "Reasonably Selenium#3815"